To run server app install and run Neo4j and update .env file accordingly.

To run Neo4j console fire `.\bin\neo4j console` after changing to Neo4j top directory.

CREATE (:User { userid: "sidtrivedi1", firstname: "Sid",lastname: "Trivedi", email:'sidtrivedi@abc.xyz', date_joined: 1519774502462 })

MATCH (s:User) WHERE ID(s) = 174 RETURN s

MATCH (n)
DETACH DELETE (n)

```{
	"userid": "sidtrivedi1",
    "text": "Buy a milk bag"
}```
