const request = require('superagent');
const BACKEND = `http://${process.env.BACKEND_HOST}:\
${process.env.BACKEND_PORT}`
const USER = `${process.env.USER}`
const TIMEOUT = 1000
export const backendAPI = {
  getNotes(){
    return new Promise((resolve, reject)=>{
      request.get(`${BACKEND}/note`)
      .then((response)=>{
        resolve(response.body)
      })
      .catch((error)=>{
        reject({msg: 'Failed to fetch all notes'});
      })
    })
  },

  addNote(text){
    return new Promise((resolve, reject)=>{
      request.post(`${BACKEND}/note/add`)
      .send({userid: USER, text })
      .then((response)=>{
        resolve(response.body)
      }).catch((error)=>{
        reject({msg: 'Failed to add note'});
      })
    })
  },
  updateNote(note){
    return new Promise((resolve, reject)=>{
      request.put(`${BACKEND}/note/${note.noteID}`)
      .send({userid: USER, text: note.noteText })
      .then((response)=>{
        resolve(response.body)
      }).catch((error)=>{
        reject({msg: 'Failed to add note'});
      })
    })
  }
  // deleteNote(noteID){
  //
  // }
}
