//action types
export const ADD_NOTE = 'ADD_NOTE'
export const NOTE_ADDED = 'NOTE_ADDED'
export const UPDATE_NOTE = 'UPDATE_NOTE'
export const NOTE_UPDATED = 'NOTE_UPDATED'
export const SET_ACTIVE_NOTE = 'SET_ACTIVE_NOTE'
export const FETCH_ALL_NOTES = 'FETCH_ALL_NOTES'
export const NOTES_FETCHED = 'NOTES_FETCHED'

// action creactors
export const addNote = noteText => {
  return {
    type: ADD_NOTE,
    noteText
  };
}

export const updateNote = note => {
  return {
    type: UPDATE_NOTE,
    note
  };
}
export const noteUpdated = note => {
  return {
    type: NOTE_ADDED,
    ...note
  }
}

export const loadNote = activeNoteId => {
  return {
    type: SET_ACTIVE_NOTE,
    activeNoteId
  };
}

export const fetchNotes = () => {
  return {
    type: FETCH_ALL_NOTES
  }
}
export const notesFetched = (notes) => {
  return {
    type: NOTES_FETCHED,
    notes
  }
}

export const noteAdded = (note) => {
  return {
    type: NOTE_ADDED,
    ...note
  }
}
