import React, {Component} from 'react'

import Button from './button'
//All style imports
import '../styles/note.less'

export default class Note extends Component{
  constructor(props) {
    super(props);
    let {activeNote} = this.props;

    this.state = {value: ''};

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  componentWillReceiveProps(nextProps){
    let {activeNote} = nextProps;
    this.setState({value: activeNote?activeNote.noteText:''})
  }
  render(){
    return (
      <div className="note">
        <textarea
          ref={(textarea)=>{this.noteTextArea = textarea}}
          value={this.state.value}
          onChange={this.handleChange}
          >
        </textarea>
        {!this.props.activeNote && <Button onClick={this.props.onAddNote}>
          <i className={this.props.faIcon} aria-hidden={true} />
          Add a new note
        </Button>}
        {this.props.activeNote && <Button onClick={this.props.onUpdateNote}>
          <i className={this.props.faIcon} aria-hidden={true} />
          Update note
        </Button>}
      </div>
    )
  }
}
