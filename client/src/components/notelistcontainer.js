import React, {Component} from 'react'

// All component imports
import VisibleNoteList from '../containers/visiblenotelist'

//All style imports
import '../styles/notelistcontainer.less'
import 'font-awesome/css/font-awesome.min.css';

export default class NoteListContainer extends Component{
  render(){
    return(
      <div className="noteListContainer">
        <h3> Notes </h3>
        <VisibleNoteList />
      </div>
    )
  }
}
