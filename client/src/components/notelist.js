import React, {Component} from 'react'
import PropTypes from 'prop-types'
// All component imports
import NoteListItem from './notelistitem'

//All style imports
import '../styles/notelist.less'

export default class NoteList extends Component{
  componentDidMount(){
    this.props.fetchNotes();
    console.log(this.props);
  }
  render(){
    let {notes, onNoteListItemClick} = this.props;
    var noteItems = notes.map((note)=>{
      return (
        <NoteListItem
          key={note.noteID}
          onClick={
            ()=>onNoteListItemClick(note.noteID)
          }
          >
          {note.noteID}{`. `}
          {note.noteText.substring(0,15)}
          {note.noteText.length > 15 ? '....' : '' }
        </NoteListItem>
      );
    });
    return(
      <ul className="noteList">
        {noteItems}
      </ul>
    )
  }
}

NoteList.propTypes = {
  notes: PropTypes.arrayOf(
    PropTypes.shape({
      noteID: PropTypes.number.isRequired,
      noteText: PropTypes.string.isRequired
    }).isRequired
  ).isRequired,
  onNoteListItemClick: PropTypes.func.isRequired
}
