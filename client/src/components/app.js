import React, {Component} from 'react';

// All component imports
import Dashboard from './dashboard'

//All style imports
import '../styles/master.less'

export default class App extends Component {
  render(){
    return (
      <Dashboard />
    )
  }
}
