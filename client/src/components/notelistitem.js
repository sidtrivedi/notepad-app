import React, {Component} from 'react'

//All style imports
import '../styles/notelistitem.less'

export default class NoteListItem extends Component{
  constructor(props){
    super(props)
  }
  render(){
    return(
      <li className="noteListItem" {...this.props}>
      </li>
    )
  }
}
