import React, {Component} from 'react'

// All component imports
import NoteListContainer from './notelistcontainer'
import AddNote from '../containers/addnote'
//All style imports
import '../styles/dashboard.less'

export default class Dashboard extends Component {
  render(){
    return (
      <div className="dashboard">
        <NoteListContainer />
        <AddNote />
      </div>
    )
  }
}
