import React, {Component} from 'react'
import { connect } from 'react-redux'

//import required actions
import { loadNote,fetchNotes } from '../actions'

//import presentational component
import NoteList from '../components/notelist'

class VisibleNoteList extends Component{
  render(){
    return (
      <NoteList {...this.props}/>
    )
  }
}

function mapStateToProps(state) {
  return {
    notes: state.notes
  }
}

function mapDispatchToProps (dispatch) {
  return {
    onNoteListItemClick: (noteID) => {
      dispatch(loadNote(noteID));
    },
    fetchNotes: () =>{
      dispatch(fetchNotes())
    }
  }
}

VisibleNoteList = connect(
  mapStateToProps,
  mapDispatchToProps
)(VisibleNoteList)
 
export default VisibleNoteList
