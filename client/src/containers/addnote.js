import React, {Component} from 'react'
import { connect } from 'react-redux'

import Note from '../components/note'
import { addNote, updateNote, loadNote } from '../actions'

class AddNote extends Component{
  onAddNote(e){
    e.preventDefault()
    if (!this.note.noteTextArea.value.trim()) {
      return;
    }
    this.props.dispatch(
      addNote(this.note.state.value)
    );
    this.note.setState({value: ''});
  }

  onUpdateNote(e){
    e.preventDefault()
    if (!this.note.noteTextArea.value.trim()) {
      return;
    }
    let updatedNote = {
      noteID: this.props.activeNote.noteID,
      noteText: this.note.state.value
    }
    this.props.dispatch(
      updateNote(updatedNote)
    );
    this.props.dispatch(
      loadNote(-1)
    );
  }
  render(){
    return (
      <Note
        ref={(node) => {this.note = node}}
        activeNote={this.props.activeNote}
        onAddNote={this.onAddNote.bind(this)}
        onUpdateNote={this.onUpdateNote.bind(this)}
      />
    )
  }
}
function mapStateToProps(state) {
  return {
    activeNote: state.notes.find(
      note=>note.noteID === state.activeNote
    )
  }
}
AddNote = connect(mapStateToProps)(AddNote)
 
export default AddNote
