import { call, put, takeEvery, fork, all } from 'redux-saga/effects'
import { backendAPI } from '../services'
import {
  FETCH_ALL_NOTES,
  NOTES_FETCHED,
  ADD_NOTE,
  NOTE_ADDED,
  UPDATE_NOTE,
  NOTE_UPDATED
} from '../actions'


export function* fetchAllNotes() {
  const notes = yield call(backendAPI.getNotes)
  yield put({type: NOTES_FETCHED, notes})
}

//watcher for fetchAllNotes
export function* watchGetNotes() {
  yield takeEvery(FETCH_ALL_NOTES, fetchAllNotes)
}

export function* addNote({noteText}) {
  const note = yield call(backendAPI.addNote, noteText)
  yield put({type: NOTE_ADDED, ...note})
}

  //watcher for addNote
export function* watchAddNote() {
  yield takeEvery(ADD_NOTE, addNote)
}

export function* updateNote({note}) {
  const updatedNote = yield call(backendAPI.updateNote, note)
  yield put({type: NOTE_UPDATED, ...updatedNote})
}
//watcher for updateNote
export function* watchUpdateNote() {
  yield takeEvery(UPDATE_NOTE, updateNote)
}

export default function* rootSaga(){
  yield all([
    fork(watchGetNotes),
    fork(watchAddNote),
    fork(watchUpdateNote)
  ])
}
