export default function activeNote(state=-1, action) {
  switch (action.type) {
    case 'SET_ACTIVE_NOTE':
      return action.activeNoteId;
      break;
    default:
      return state;
  }
}
