import { combineReducers } from 'redux'
import notes from './notes'
import activeNote from './activenote'

const notepadApp = combineReducers({
  notes,
  activeNote
})
 
export default notepadApp
