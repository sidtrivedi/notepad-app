const notes = (state = [], action) => {
  switch (action.type) {
    case 'NOTE_ADDED':
      return [
        {
          noteID: action.noteid,
          noteText: action.text,
          writtenOn: action.written_on
        },
        ...state
      ]
      case 'NOTE_UPDATED':
        return state.map(note=>
          (note.noteID === action.noteid) ?
          {...note, noteText: action.text }
          : note
        )
      case 'NOTES_FETCHED':
        let refinedNotes = action.notes.map(note=>{
          return {noteID: note.noteid,
          noteText: note.text,
          writtenOn: note.written_on}
        })
        return state.concat(refinedNotes)
    default:
      return state
  }
}
 
export default notes
