const neo4j_uri = `${process.env.NEO4J_PROTOCOL}://\
${process.env.NEO4J_HOST}:${process.env.NEO4J_PORT}`
const neo4j_user = process.env.NEO4J_USERNAME
const neo4j_password = process.env.NEO4J_PASSWORD

const neo4j = require('neo4j-driver').v1;

const driver = neo4j.driver(
  neo4j_uri,
  neo4j.auth.basic(
    neo4j_user,
    neo4j_password
  )
);

function addNote(newNote) {
  let session = driver.session();
  let cypher = `MATCH (u:User{userid:'${newNote.userid}'})
  CREATE (n:Note { text:'${newNote.noteText}'})
  CREATE (u)-[w:wrote{wrote_on:${Date.now()}}]->(n)
  RETURN n.text as text, id(n) as noteid, w.wrote_on as written_on`;
  return session.run(
    cypher
  ).then(result => {
    session.close();
    let recordKeys = result.records[0].keys;
    let recordFields = result.records[0]._fields;
    let index = 0;
    var addedNote = {};
    recordKeys.map(key=>{
      addedNote = Object.assign({}, addedNote,
        {[key]: recordFields[index++]}
      );
    });
    return addedNote;
  }).catch(error => {
    session.close();
    throw error;
  });
}

function getNotes(query) {
  let session = driver.session();
  const start= (parseInt(query.start) || 1) - 1;
  const order = ['asc', 'desc'].includes(query.order) ?query.order : 'desc';
  const limit = query.limit?
  `LIMIT ${Math.abs(parseInt(query.limit)) +
     Math.abs(start)}` : '';
  let cypher = `MATCH (u:User)-[w:wrote]->(n:Note)
RETURN n.text as text, id(n) as noteid, w.wrote_on as written_on
ORDER BY w.written_on ${order} ${limit}`;
  return session.run(
    cypher
  ).then(result => {
    session.close();

    return result.records.map(rawNote=>{
      let recordKeys =rawNote.keys;
      let recordFields = rawNote._fields;
      let index = 0;
      let note = {};
      recordKeys.map(key=>{
        note = Object.assign({}, note,
          {[key]: recordFields[index++]}
        );
      });
      return note
    })
  }).catch(error => {
    session.close();
    throw error;
  });
}

function getNote(noteid) {
  let session = driver.session();
    let cypher = `MATCH (u:User)-[w:wrote]->(n:Note)
WHERE ID(n) = ${noteid}
RETURN n.text as text, id(n) as noteid, w.wrote_on as written_on`;
    return session.run(
      cypher
    ).then(result => {
      session.close();
      if (!result.records.length) return null;
      let recordKeys = result.records[0].keys;
      let recordFields = result.records[0]._fields;
      let index = 0;
      var note = {};
      recordKeys.map(key=>{
        note = Object.assign({}, note,
          {[key]: recordFields[index++]}
        );
      });
      return note;
    }).catch(error => {
      session.close();
      throw error;
    });
}
function updateNote(id, text) {
  let session = driver.session();
  let cypher = `MATCH (u:User)-[w:wrote]->(n:Note)
WHERE ID(n) = ${id}
SET n.text = '${text}'
RETURN n.text as text, id(n) as noteid, w.wrote_on as written_on`
  return session.run(
    cypher
  ).then(result => {
    session.close();
    if (!result.records.length) return null;
    let recordKeys = result.records[0].keys;
    let recordFields = result.records[0]._fields;
    let index = 0;
    var note = {};
    recordKeys.map(key=>{
      note = Object.assign({}, note,
        {[key]: recordFields[index++]}
      );
    });
    return note;
  }).catch(error => {
    session.close();
    throw error;
  });
}

function deleteNote(id) {
  let session = driver.session();
  let cypher = `MATCH (u:User)-[w:wrote]->(n:Note)
WITH n, n.text as text, id(n) as id, w.wrote_on as written_on
WHERE id=${id}
DETACH DELETE n
RETURN text, id, written_on`
  return session.run(
    cypher
  ).then(result => {
    session.close();
    if (!result.records.length) return null;
    let recordKeys = result.records[0].keys;
    let recordFields = result.records[0]._fields;
    let index = 0;
    var note = {};
    recordKeys.map(key=>{
      note = Object.assign({}, note,
        {[key]: recordFields[index++]}
      );
    });
    return note;
  }).catch(error => {
    session.close();
    throw error;
  });
}

exports.addNote = addNote;
exports.getNotes = getNotes;
exports.getNote = getNote;
exports.updateNote = updateNote;
exports.deleteNote = deleteNote;
