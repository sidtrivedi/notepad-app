const express = require('express')
var bodyParser = require('body-parser');

const app = express()
app.use(bodyParser.json());

var cors = require('cors');
app.use(cors());

const _ = require('lodash')

var env = require('node-env-file');
env(__dirname + '/../.env');

var neo4japi = require('./neo4japi');
// # Add a note
// POST /note/add
app.post('/note/add', (req, res) => {
  const noteText = req.body.text;
  const userid = req.body.userid;
  let newNote = {noteText,userid}

  neo4japi
  .addNote(newNote)
  .then(resultNote=>{
    if (!resultNote) res.status(204).send();
    else res.status(200).send(resultNote);
  }).catch(error=>{
    console.error(error);
    res.status(500).send(error);
  })
})

// # Get notes, optionally specifying GET parameters where:
// # limit - indicating the maximum number of notes to get; if unspecified it gets all notes
// # order - specifies what order to sort the notes, based on creation time which can be either "asc" or "desc"; if unspecified, defaults to descending
// # start - specifies where in the sorted notes to begin getting notes; if unspecified, defaults to 1
// # limit - specified the maximum number of notes to get
// GET /note?limit=10&start=1&order=asc
app.get('/note', (req, res) => {
  neo4japi
  .getNotes(req.query)
  .then(result=>{
    if (!result) res.status(204).send();
    else res.status(200).send(result);
  }).catch(error=>{
    console.error(error);
    res.status(500).send(error);
  })
})

// # View a note with a given id
// GET /note/:id
app.get('/note/:id', (req, res) => {
  const noteid = req.params.id;
  neo4japi
  .getNote(noteid)
  .then(result=>{
    if (!result) res.status(204).send();
    else res.status(200).send(result);
  }).catch(error=>{
    console.error(error);
    res.status(500).send(error);
  })
})

// # Update a note with a given id
// PUT /note/:id
app.put('/note/:id', (req, res) => {
  const noteid = req.params.id
  const noteText = req.body.text
  neo4japi
  .updateNote(noteid, noteText)
  .then(result=>{
    if (!result) res.status(204).send();
    else res.status(200).send(result);
  }).catch(error=>{
    console.error(error);
    res.status(500).send(error);
  })
})

// # Delete a note with a given id
// DELETE /note/:id
app.delete('/note/:id', (req, res) => {
  const noteid = req.params.id
  neo4japi
  .deleteNote(noteid)
  .then(result=>{
    if (!result) res.status(204).send();
    else res.status(200).send(result);
  }).catch(error=>{
    console.error(error);
    res.status(500).send(error);
  })
})
app.listen(3000, () => console.log('Server app listening on port 3000!'))
